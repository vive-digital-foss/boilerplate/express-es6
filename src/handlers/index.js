import path from 'path'
import fs from 'fs'

export default () => {
  const routes = []
  fs.readdirSync(path.join(__dirname, '')).forEach(file => {
    if (file !== 'index.js') {
      const controllers = require(path.join(__dirname, file)).default()
      routes.push(controllers)
    }
  })
  return routes
}

import http from 'http'
import consola from 'consola'

import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import cors from 'cors'

import handlers from './handlers'
import { PORT, NAME, VERSION } from './utils/secrets'

// initial express
let app = express()
app.server = http.createServer(app)
if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('dev'))
}
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
app.use(cors())
app.use('/', handlers())
consola.info(`${NAME} version: ${VERSION}`)
if (process.env.NODE_ENV !== 'test') {
  app.server.listen(PORT || 3000, () => {
    consola.info(`Started on port ${app.server.address().port}`)
  })
}
export default app

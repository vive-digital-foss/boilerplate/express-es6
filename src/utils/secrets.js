import dotenv from 'dotenv'
import consola from 'consola'
import fs from 'fs'
if (fs.existsSync('.env.example')) {
  consola.info('Using .env.example file to supply config environment variables')
  dotenv.config({ path: '.env.example' }) // you can delete this after you create your own .env file!
}

export const PORT = process.env.PORT
export const NAME = require('../../package.json').name
export const VERSION = require('../../package.json').version

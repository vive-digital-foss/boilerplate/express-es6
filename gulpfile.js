const gulp = require('gulp')
const del = require('del')
const eslint = require('gulp-eslint')
const babel = require('gulp-babel')
const spawn = require('child_process').spawn

let node // current node process

gulp.task('clean:dist', () => {
  return del('dist/*')
})

gulp.task('lint', () => {
  return gulp
    .src(['src/**/*.js', '!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
})

gulp.task('copy', () => {
  return gulp.src(['package.json']).pipe(gulp.dest('dist'))
})

gulp.task('build', ['clean:dist', 'lint', 'copy'], cb => {
  return gulp
    .src(['src/**/*.js'])
    .pipe(
      babel({
        presets: ['es2015']
      })
    )
    .pipe(gulp.dest('dist'))
})

gulp.task('dev', ['build', 'serve'], cb => {
  gulp.watch(['./src/**/*.js'], ['build', 'serve'])
})

gulp.task('serve', ['build'], () => {
  if (node) node.kill()

  node = spawn('node', ['dist/app.js'], { stdio: 'inherit' })
  node.on('close', function(code) {
    if (code === 8) gulp.log('Error detected, waiting for changes...')
  })
})

gulp.task('default', ['build', 'serve'])
